package Route

import (
	"bmg-backend-test/Controller"
	"bmg-backend-test/Middleware"
	"bmg-backend-test/docs"
	"github.com/gin-gonic/gin"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()

	v1 := r.Group("api/v1")
	{
		v1.POST("login", Controller.LoginUser)
		v1.GET("random-hero/:name", Controller.SingleRandomHero)
		v1.GET("random-hero", Controller.SingleRandomHero)
		user := v1.Group("user")
		{
			user.GET("", Controller.ListUser)
			user.POST("", Controller.AddUser)
		}
		v1.Use(Middleware.Auth)//route with authorization
		{
			v1.PUT("user/:username", Controller.EditUser)
			v1.POST("submit-referral", Controller.SubmitReferral)
		}
	}
	r.NoRoute(func(c *gin.Context) {
		c.JSON(404, gin.H{"code": 404, "message": "Page Not Found"})
	})
	docs.SwaggerInfo.BasePath = "/api/v1"
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))
	return r
}