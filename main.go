package main

import (
	"bmg-backend-test/Config"
	"bmg-backend-test/Helper"
	"bmg-backend-test/Model"
	"bmg-backend-test/Route"
	"github.com/dgraph-io/ristretto"
	"github.com/joho/godotenv"
)

func main() {
	_ = godotenv.Load()
	Config.CreateDb()
	Config.DB, _ = Config.Con()
	_ = Config.DB.AutoMigrate(&Model.M_user{})
	Helper.CacheData, _ = ristretto.NewCache(&ristretto.Config{//initiate cache
		NumCounters: 1e7,     // number of keys to track frequency of (10M).
		MaxCost:     1 << 30, // maximum cost of cache (1GB).
		BufferItems: 64,      // number of keys per Get buffer.
	})
	r := Route.SetupRouter()
	// running
	port := "5926"
	_ = r.Run(":"+port)
}