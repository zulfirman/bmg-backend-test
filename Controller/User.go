package Controller

import (
	"bmg-backend-test/Config"
	"bmg-backend-test/Helper"
	"bmg-backend-test/Model"
	"crypto/sha256"
	"encoding/hex"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"os"
	"time"
)

var fieldQuery="username, name, email, referral_code"
// @BasePath /api/v1
// @Schemes
// @Description Search User By Name
// @Tags User
// @Accept json
// @Produce json
// @Param filters query string false "name search" default(["name","like","MIRA"])
// @Router /user [get]
func ListUser(c *gin.Context) {
	var user []Model.M_user
	qry :=Config.DB.Select(fieldQuery).Find(&user)
	if qry.Error != nil {
		Helper.Rs(c, 500, "", qry.Error, user)
		return
	}
	Helper.Rs(c, 200, "", Helper.PG.With(qry).Request(c.Request).Response(&user), "")
}
// @Description Login User
// @Schemes
// @Tags Login
// @Accept json
// @Produce json
// @Param email body string true "input" SchemaExample({"username":"mira","password":"passnya"})
// @Router /login [post]
func LoginUser(c *gin.Context) {
	var user Model.M_user
	_ = c.ShouldBindJSON(&user)
	if err := Config.DB.Select(fieldQuery).Where("username", user.Username).Where("password", hashPass(user.Password)).First(&user).Error; err != nil {
		Helper.Rs(c, 404, "Invalid Username or Password", "", "")
		return
	}
	user.Password=""
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["exp"] = time.Now().Add(time.Minute * 2).Unix()
	atClaims["profile"] = user
	sign := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, _ := sign.SignedString([]byte(os.Getenv("JWT_SECRET")))
	Helper.Rs(c, 200, "", gin.H{
		"token": token,
		"userInfo": user,
	}, "")
}
// @Description Add User
// @Schemes
// @Tags User
// @Accept json
// @Produce json
// @Param email body string true "input" SchemaExample({"username":"mira","password":"passnya","name":"mira","email":"mira@gmail.com"})
// @Router /user [post]
func AddUser(c *gin.Context) {
	var user Model.M_user
	_ = c.ShouldBindJSON(&user)
	if err := user.Vd(); err != nil {
		user.Password=""
		Helper.Rs(c, 400, "", err, user)
		return
	}
	checkUser :=true
	if err := Config.DB.Where("username", user.Username).Or("email", user.Email).First(&Model.M_user{}).Error; err != nil {
		checkUser=false//not found
	}
	if checkUser {
		user.Password=""
		Helper.Rs(c, 500, "Duplicate Data", "", user)
		return
	}
	user.Password= hashPass(user.Password)
	user.ReferralCode=Helper.UniqId()
	Config.DB.Create(&user)
	user.Password=""
	Helper.Rs(c, 200, "", user, "")
}
// @Description Edit User
// @Schemes
// @Tags User
// @Accept json
// @Produce json
// @Param email body string true "input" SchemaExample({"username":"mira","password":"passnya","name":"mira","email":"mira@gmail.com"})
// @Param username path string true "Input Username"
// @Param Authorization header string true "With the bearer"
// @Router /user/{username} [put]
func EditUser(c *gin.Context) {
	id := c.Params.ByName("username")
	var user Model.M_user
	_= c.ShouldBind(&user)
	if err := user.Vd(); err != nil {
		Helper.Rs(c, 400, "", err, user)
		return
	}
	var userCheck Model.M_user
	if err := Config.DB.Where("username", id).First(&userCheck).Error; err != nil {
		Helper.Rs(c, 404, "", "", "")
		return
	}
	user.Username=""
	if user.Password!=userCheck.Password{
		user.Password = hashPass(user.Password)
	}
	if err := Config.DB.Model(&user).Where("username", id).Updates(user).Error; err != nil {
		Helper.Rs(c, 500, "", err, user)
		return
	}
	user.Password=""
	Helper.Rs(c, 200, "", user, "")
}
// @Description Submit Referral
// @Schemes
// @Tags User
// @Accept json
// @Produce json
// @Param email body string true "input" SchemaExample({"referralCode":"69624d175f2b43e"})
// @Param Authorization header string true "With the bearer"
// @Router /submit-referral [post]
func SubmitReferral(c *gin.Context) {
	var user Model.M_user
	_ = c.ShouldBindJSON(&user)
	if err := Config.DB.Where("referral_code", user.ReferralCode).First(&user).Error; err != nil {
		Helper.Rs(c, 200, "", false, "")
		return
	}
	me :=Helper.Me(c)
	if user.Username==me.Username{
		Helper.Rs(c, 200, "", false, "")
		return
	}
	Helper.Rs(c, 200, "", true, "")
}

func hashPass(pass string) string {
	hasher :=sha256.New()
	hasher.Write([]byte(pass))
	return hex.EncodeToString(hasher.Sum(nil))
}