package Controller

import (
	"bmg-backend-test/Helper"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/gojsonq/v2"
)

// @Description Single Random Hero By Name
// @Tags Random Hero
// @Accept json
// @Produce json
// @Param name path string true "Input Name Of Hero eg. Noctur"
// @Router /random-hero/{name} [get]
func SingleRandomHero(c *gin.Context) {
	id :=c.Param("name")
	value, found := Helper.CacheData.Get("singleRandomHeroid"+id)
	if found {//return cached data
		Helper.Rs(c,200,"", value,"")
		return
	}
	getData, _ := Helper.GetReq("https://ddragon.leagueoflegends.com/cdn/6.24.1/data/en_US/champion.json", "")
	jq := Helper.ReadJson(getData.String(),"data")
	if id==""{//if empty = get all
		Helper.Rs(c, 200, "",jq, "")
		Helper.CacheData.Set("singleRandomHeroid"+id, jq, 1)//create cache
		return
	}

	listData := []interface{}{}
	for _, v := range jq.(map[string]interface{}) {//remove all key of hero name
		listData=append(listData, v)
	}
	listDataJson, _ :=json.Marshal(listData)
	search := gojsonq.New().FromString(string(listDataJson)).Where("name","contains",id).First()
	Helper.Rs(c, 200, "",search, "")
	Helper.CacheData.Set("singleRandomHeroid"+id, search, 1)//create cache
}
// @Description Single Random Hero
// @Tags Random Hero
// @Accept json
// @Produce json
// @Router /random-hero [get]
func SingleRandomHeroAll(c *gin.Context) {}