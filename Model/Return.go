package Model

type Return struct {
	Status      int         `json:"status"`
	Message     string      `json:"message"`
	Content     interface{} `json:"content"`
	Others      interface{} `json:"others"`
	DateRequest string      `json:"dateRequest"`
	Path        string      `json:"path"`
}
