package Model

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

type M_user struct {
	Username     string `json:"username" gorm:"primaryKey" `
	Password     string `json:"password"`
	Name         string `json:"name"`
	Email        string `json:"email" gorm:"index:idx_email,unique"`
	ReferralCode string `json:"referralCode"`
}

func (a M_user) Vd() error {
	var req = validation.Required
	return validation.ValidateStruct(&a,
		validation.Field(&a.Email, req, is.Email),
		validation.Field(&a.Username, req),
		validation.Field(&a.Password, req, is.Alphanumeric),
		validation.Field(&a.ReferralCode, is.Alphanumeric),
	)
}
func (M_user) TableName() string {
	return "m_user"
}
