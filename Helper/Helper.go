package Helper

import (
	"bmg-backend-test/Model"
	"fmt"
	"github.com/dgraph-io/ristretto"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/go-resty/resty/v2"
	"github.com/mintance/go-uniqid"
	"github.com/mitchellh/mapstructure"
	"github.com/morkid/paginate"
	"github.com/thedevsaddam/gojsonq/v2"
	"net/http"
	"os"
	"strings"
	"time"
)

var PG = paginate.New()
var CacheData *ristretto.Cache//cache variable

func Rs(c *gin.Context, Status int, Message string, Content interface{}, Others interface{}) {
	//custom return json function
	var Return Model.Return
	empty :=struct {}{}
	Return.Status =Status
	if Message == "" {
		Return.Message = http.StatusText(Status)
	} else {
		Return.Message = Message
	}
	if Content == "" {
		Return.Content = empty
	} else {
		Return.Content = Content
	}
	if Others==nil||Others==""{
		Return.Others = empty
	}else {
		Return.Others = Others
	}
	Return.DateRequest = time.Now().Format("2006-01-02 15:04:05")
	urlPath :=strings.ReplaceAll(c.Request.URL.Path,"api/","")//base url use c.Request.Host
	if c.Request.URL.RawQuery!=""{
		urlPath = urlPath+"?"+c.Request.URL.RawQuery
	}
	Return.Path=urlPath
	c.JSON(Status, Return)
}
type jwtStruct struct {
	Model.M_user
	Token  string
	Error string
	jwt.StandardClaims
}
func Me(c *gin.Context) jwtStruct {//check if jwt token is valid
	tokenString := c.Request.Header.Get("Authorization")
	tokenString=strings.Replace(tokenString,"Bearer ","",1)
	var ambil jwtStruct
	claims := jwt.MapClaims{}
	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		if jwt.SigningMethodHS256 != token.Method {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("JWT_SECRET")), nil
	})
	mapstructure.Decode(claims["profile"], &ambil.M_user)//parse result of jwt claim into struct
	if token == nil && err != nil || !token.Valid{//not verified
		ambil.Token="false"
		ambil.Error=err.Error()
		return ambil
	}
	ambil.Token=tokenString
	return ambil
}
func GetReq(Url string, token string) (*resty.Response, error) {
	client := resty.New()
	resp, err := client.R().EnableTrace().SetAuthToken(token).Get(Url)
	if err != nil {
		fmt.Println(err)
	}
	return resp, err
}
func ReadJson(jsonString string, path string) interface{} {//from json string to json
	if path==""{
		return gojsonq.New().FromString(jsonString).Get()
	}
	return gojsonq.New().FromString(jsonString).Find(path)
}
func UniqId() string{//return string of unique id
	return uniqid.New(uniqid.Params{"69", false})
}