package Config

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"os"
	"time"
)

var DB *gorm.DB

func Con() (*gorm.DB, error) {
	//connection to database
	host :=os.Getenv("HOSTDB")
	dbname :=os.Getenv("NAMEDB")
	user :=os.Getenv("USERDB")
	pass :=os.Getenv("PASSDB")
	port :=os.Getenv("PORTDB")
	dsn := "host="+host+" user="+user+" password="+pass+" dbname="+dbname+" port="+port+" TimeZone=Asia/Shanghai"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		SkipDefaultTransaction: true,
		PrepareStmt: true,
		NamingStrategy: schema.NamingStrategy{},
		NowFunc: func() time.Time {
			return time.Now().Local()
		},
	})
	if(err!=nil){
		fmt.Println(err)
		os.Exit(1);
	}
	return db, nil
}
func CreateDb()  {
	nameDb :=os.Getenv("NAMEDB")
	// check if db exists
	connStr := fmt.Sprintf("user=%s password=%s host=%s port=%s dbname=%s sslmode=disable",
		os.Getenv("USERDB"),
		os.Getenv("PASSDB"),
		os.Getenv("HOSTDB"),
		os.Getenv("PORTDB"),
		"postgres")
	// connect to the postgres db just to be able to run the create db statement
	db, err := gorm.Open(postgres.Open(connStr), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Silent)})
	if err != nil {
		fmt.Println(err)
	}
	stmt := fmt.Sprintf("SELECT * FROM pg_database WHERE datname = '%s';", nameDb)
	rs := db.Raw(stmt)
	if rs.Error != nil {
		fmt.Println(rs.Error)
	}
	// if not create it
	var rec = make(map[string]interface{})
	if rs.Find(rec); len(rec) == 0 {
		stmt := fmt.Sprintf("CREATE DATABASE %s;", nameDb)
		if rs := db.Exec(stmt); rs.Error != nil {
			fmt.Println(rs.Error)
		}
		// close db connection
		sql, err := db.DB()
		defer func() {
			_ = sql.Close()
		}()
		if err != nil {
			fmt.Println(err)
		}
	}
}