package Middleware

import (
	"bmg-backend-test/Helper"
	"github.com/gin-gonic/gin"
)

func Auth(c *gin.Context) {
	checkAuth :=Helper.Me(c)
	if checkAuth.Token == "false" {//not verified
		result := gin.H{
			"message": "token not valid",
			"error": checkAuth.Error,
		}
		Helper.Rs(c, 401, "", result, "")
		c.Abort()
		return
	}
	return
}